package nl.utwente.di.first.resources;

import nl.utwente.di.first.dao.BikeDao;
import nl.utwente.di.first.model.Bike;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Path("/bikes")
public class BikesResource {

    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Bike> getBikes(){
        List<Bike> bikes = new ArrayList<Bike>();
        bikes.addAll(BikeDao.instance.getModel().values());
        return bikes;
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String orderBike(@FormParam("id") String id, @Context HttpServletResponse servletResponse){
        if (getBikes().contains(id)) return "success";
        else return "fail";
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void createBike(Bike bike){

    }

    @Path("{bike}")
    public BikeResource getBikes(@PathParam("bike") String id) {
        return new BikeResource(uriInfo, request, id);
    }
}
