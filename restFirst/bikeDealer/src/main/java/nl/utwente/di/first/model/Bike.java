package nl.utwente.di.first.model;

public class Bike {
    String id;
    String ownerName;
    String colour;
    String gender;

    public Bike(String id, String ownerName, String colour, String gender) {
        super();
        this.id = id;
        this.ownerName = ownerName;
        this.colour = colour;
        this.gender = gender;
    }

    public Bike(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getGender(){
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOwnerName(){
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getColour(){
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }
}
