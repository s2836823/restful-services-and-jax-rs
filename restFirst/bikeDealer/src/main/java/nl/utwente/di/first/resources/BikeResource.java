package nl.utwente.di.first.resources;

import nl.utwente.di.first.dao.BikeDao;
import nl.utwente.di.first.model.Bike;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.JAXBElement;

public class BikeResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    String id;
    public BikeResource(UriInfo uriInfo, Request request, String id) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.id = id;
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Bike getBikeDetails(){
        Bike bike = BikeDao.instance.getModel().get(id);
        if (bike == null)
            throw new RuntimeException("Get: Bike with id " + id);
        return bike;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public Response putBike(JAXBElement<Bike> bike){
        Bike c  = bike.getValue();
        return putAndGetResponse(c);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public Bike updateBike(Bike bike){
        deleteBike(bike.getId());
        return getBikeDetails();
    }

    @DELETE
    public void deleteBike(String id){
        Bike c = BikeDao.instance.getModel().remove(id);
        if (c==null)
            throw new RuntimeException("Delete: Bike with ID " + id + " not found");
    }

    private Response putAndGetResponse(Bike bike){
        Response res;
        if (BikeDao.instance.getModel().containsKey(bike.getId())){
            res = Response.noContent().build();
        } else {
            res = Response.created(uriInfo.getAbsolutePath()).build();
        }
        BikeDao.instance.getModel().put(bike.getId(), bike);
        return res;
    }
}
