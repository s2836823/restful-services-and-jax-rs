package nl.utwente.di.first.dao;

import nl.utwente.di.first.model.Bike;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

public enum BikeDao {
    instance;
    private Map<String, Bike> contentProvider = new HashMap<String, Bike>();

    private BikeDao(){
        Bike bike = new Bike("1", "John", "Green", "Male");
        contentProvider.put("1", bike);
        bike = new Bike("2", "Michael", "Blue", "Male");
        contentProvider.put("2", bike);
    }
    public Map<String, Bike> getModel(){
        return contentProvider;
    }
}
